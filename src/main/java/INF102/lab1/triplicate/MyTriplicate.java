package INF102.lab1.triplicate;

import java.util.Collections;
import java.util.List;

public class MyTriplicate<T> implements ITriplicate<T> {

    @Override
    public T findTriplicate(List<T> list) {
    	int n = list.size();
    	Collections.sort(list, null);
    	for (int i = 0; i < n-2; i++) {
			T one = list.get(i);
			T two = list.get(i+1);
			T three = list.get(i+2);
			if (one.equals(two) && one.equals(three)) {
				System.out.println(one);
				return one;
			}
		}
		return null;
    }
    
}
